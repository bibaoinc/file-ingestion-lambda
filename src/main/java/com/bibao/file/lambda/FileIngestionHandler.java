package com.bibao.file.lambda;

import java.io.*;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.bibao.file.dynamodb.dao.TrainingDaoImpl;
import com.bibao.file.dynamodb.entity.TrainingEntity;

public class FileIngestionHandler implements RequestHandler<S3Event, Void> {

    private AmazonS3 s3 = AmazonS3ClientBuilder.standard().build();

    public FileIngestionHandler() {}

    // Test purpose only.
    public FileIngestionHandler(AmazonS3 s3) {
        this.s3 = s3;
    }

    @Override
    public Void handleRequest(S3Event event, Context context) {
        context.getLogger().log("Start training file ingestion process");

        S3EventNotification.S3EventNotificationRecord record = event.getRecords().get(0);
		String bucketName = record.getS3().getBucket().getName();
		String keyName = record.getS3().getObject().getKey();
		context.getLogger().log("Bucket Name: " + bucketName);
		context.getLogger().log("Key Name: " + keyName);
		S3Object response = s3.getObject(new GetObjectRequest(bucketName, keyName));
		if (keyName.startsWith("enroll")) {
			InputStream fileDataInputStream = response.getObjectContent();
			context.getLogger().log("Start ingesting file " + keyName);
			ingestFile(fileDataInputStream);
		}
        return null;
    }
    
    private void ingestFile(InputStream inputStream) {
    	try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
    		String line = reader.readLine();
            while ((line = reader.readLine())!=null) {
            	saveToDynamoDB(line);
            }
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    }
    
    private void saveToDynamoDB(String line) {
    	TrainingDaoImpl trainingDao = TrainingDaoImpl.getInstance();
    	String[] tokens = line.split(",");		// csv file
    	TrainingEntity entity = new TrainingEntity();
    	entity.setPK(TrainingEntity.DEFAULT_PK);
    	Date createTimestamp = new Date();
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		entity.setSK(sdf.format(createTimestamp));
		entity.setCreateTimestamp(createTimestamp);
		entity.setStudentName(tokens[0]);
		entity.setTrainingFee(new BigDecimal(tokens[2]));
		entity.setCourse(tokens[1]);
    	trainingDao.enrollTraining(entity);
    }
    
}