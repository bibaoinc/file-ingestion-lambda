package com.bibao.file.dynamodb.dao;

import com.bibao.file.dynamodb.entity.TrainingEntity;

public interface TrainingDao {
	public void enrollTraining(TrainingEntity entity);
}
