package com.bibao.file.dynamodb.dao;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTableMapper;
import com.bibao.file.dynamodb.entity.TrainingEntity;

public class TrainingDaoImpl extends AbstractDao implements TrainingDao {
	private static TrainingDaoImpl instance;
	
	private DynamoDBTableMapper<TrainingEntity, String, String> trainingEntityMapper;
	
	private TrainingDaoImpl() {
		super();
		trainingEntityMapper = dynamoDBMapper.newTableMapper(TrainingEntity.class);
	}
	
	public static TrainingDaoImpl getInstance() {
		if (instance==null) {
			instance = new TrainingDaoImpl();
		}
		return instance;
	}
	
	@Override
	public void enrollTraining(TrainingEntity entity) {
		trainingEntityMapper.save(entity);
	}
}
